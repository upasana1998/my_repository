import matplotlib.pyplot as plt
from matplotlib import interactive
import csv
import itertools
def numvertices():
	num_V = []
	num_E = []
	num_EW = []
	time = []
	for i in range(1969,2015):
		string = '/home/upasana/upasana_dutta/research_work/Clean/DB/1967_'+str(i)+'.net'
		with open(string, 'r') as f:
			x = list(f.readline().split())
			num_V.append(int(x[1]))

	for i in range(1969,2015):
		string = '/home/upasana/upasana_dutta/research_work/Clean/DB/1967_'+str(i)+'.net'
		with open(string, 'r') as f:
			counte = 0
			x = f.readlines()	#x is a list where each element is a string representing an edge with its weight
			num_E.append(len(x)-num_V[i-1969]-1)
			for k in range(num_V[i-1969]+1,len(x)):
				weight = int(list(x[k].split())[2])
				counte+=weight
			num_EW.append(counte)
	oldE_plot = []
	added = []
	same = []		
	count_oof = []
	count_oor = []
	count_on = []
	count_nn = []
	for i in range(1969,2014):
		time.append(int(i+1))
		#oldE_plot.append(num_EW[i - 1969])
		string = '/home/upasana/upasana_dutta/research_work/Clean/DB/1967_'+str(i)+'.net'
		string2 = '/home/upasana/upasana_dutta/research_work/Clean/DB/1967_'+str(i+1)+'.net'	
		with open(string, 'r') as f:
			with open(string2, 'r') as f2:
				countOOR = 0
				countOOF = 0
				SAME = 0
				countON = 0
				countNN = 0
				mydict = {}
				p = f.readlines()
				for k in range(num_V[i-1969]+1,len(p)):
					x = p[k]
					x_L = list(x.split())
					#print(len(x_L))
					mystr = x_L[0]+"_"+x_L[1]
					mydict[mystr] = int(x_L[2])
				p = f2.readlines()
				for k in range(num_V[i-1969+1]+1,len(p)):
					x = p[k]
					x_L = list(x.split())
					#print(len(x_L))
					mystr = x_L[0]+"_"+x_L[1]
					if mystr in mydict.keys():
						if int(x_L[2]) == mydict[mystr]:
							SAME+=1
						elif int(x_L[2]) > mydict[mystr]:
							countOOR+=1
							
					else:
						if (int(x_L[0])>num_V[i - 1969] and int(x_L[1])<=num_V[i - 1969]) or (int(x_L[0])<=num_V[i - 1969] and int(x_L[1])>num_V[i - 1969]):
							countON+=int(x_L[2])
						if (int(x_L[0])<=num_V[i - 1969] and int(x_L[1])<=num_V[i - 1969]):
							countOOF+=int(x_L[2])
							
						if (int(x_L[0])>num_V[i - 1969] and int(x_L[1])>num_V[i - 1969]):
							countNN+=int(x_L[2])

				#added.append(w)
				same.append(countOOF+countOOR)
				count_oof.append(countOOF)				
				count_oor.append(countOOR)
				count_on.append(countON)						
				count_nn.append(countNN)



	#fig1=plt.plot(C_values, combined_score)
	plt.title("Timestamp vs. New Co-authorships")
	#plt.subplot(2, 1, 1)
	plt.xlabel('Timestamp')
	plt.ylabel('New Co-authorships ')
	plt.plot(time, count_oof, 'b-',label='Co-authership between Fresh Old-Old',marker='o', markerfacecolor='black', markersize=3)
	for a,b in zip(time, count_oof): 
    		plt.text(a, b, str(b),fontsize=7)
	plt.plot(time, count_oor, 'g-',label='Co-authership between Repeating Old-Old',marker='o', markerfacecolor='black', markersize=3)
	for a,b in zip(time, count_oor): 
    		plt.text(a, b, str(b),fontsize=7)
	#plt.legend()
	plt.plot(time, count_on, 'y-',label='Co-authership between Old-New',marker='o', markerfacecolor='black', markersize=3)
	for a,b in zip(time, count_on): 
    		plt.text(a, b, str(b),fontsize=7)
	plt.plot(time, count_nn, 'r-',label='Co-authership between New-New',marker='o', markerfacecolor='black', markersize=3)
	for a,b in zip(time, count_nn): 
    		plt.text(a, b, str(b),fontsize=7)
	'''
	plt.subplot(2, 1, 2)
	plt.xlabel('Timestamp')
	plt.ylabel('New Co-authorships ')
	plt.plot(time, count_on, 'y-',label='Co-authership between Old-New',marker='o', markerfacecolor='black', markersize=3)
	for a,b in zip(time, count_on): 
    		plt.text(a, b, str(b),fontsize=7)
	plt.plot(time, count_nn, 'r-',label='Co-authership between New-New',marker='o', markerfacecolor='black', markersize=3)
	for a,b in zip(time, count_nn): 
    		plt.text(a, b, str(b),fontsize=7)
	'''
	plt.legend()
	plt.show()

	return count_oof,count_oor,count_on,count_nn, num_V, num_EW
		
def alter_csv():
	with open('DB-Network_Family-metrics.csv', 'w') as f:
		writer =  csv.writer(f)
		x = numvertices()
		lyear = []
		lyear.append('Timestamp')
		for i in range(1969,2015):
			lyear.append(i)
		l1 = ['old-old repeating',0]+x[1]
		l2 = ['old-old fresh',0]+x[0]
		l3 = ['old-new',0]+x[2]
		l4 = ['new-new',0]+x[3]
		l5 = ['vertices']+x[4]
		l6 = ['edges']+x[5]
		rows = zip(lyear,l5,l6,l1,l2,l3,l4)
		for row in rows:
			writer.writerow(row)
		
alter_csv()
	

#x = numvertices()
#print(x)
'''
print()
print(x[1])
print()
print(x[2])
print()
print(x[3])
print()
print(x[4])
print()
'''

