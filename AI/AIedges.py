import matplotlib.pyplot as plt
from matplotlib import interactive
import csv
import itertools
def numvertices():
	num_V = []
	num_E = []
	num_EW = []
	time = []
	mylist = []
	for i in range(1971,1984,2):
		mylist.append(i)
	for i in range(1984,2015):
		mylist.append(i)
	for i in range(1969,2015):
		if i in mylist:
			string = '/home/upasana/upasana_dutta/research_work/Clean/AI/1969_'+str(i)+'.net'
			with open(string, 'r') as f:
				x = list(f.readline().split())
				num_V.append(int(x[1]))
	#print("length of num_V array = ", len(num_V))
	for i in range(len(mylist)):
		string = '/home/upasana/upasana_dutta/research_work/Clean/AI/1969_'+str(mylist[i])+'.net'
		with open(string, 'r') as f:
			counte = 0
			x = f.readlines()	
			num_E.append(len(x)-num_V[i]-1)
			for k in range(num_V[i]+1,len(x)):
				weight = int(list(x[k].split())[2])
				counte+=weight
			num_EW.append(counte)
	oldE_plot = []
	added = []
	same = []		
	count_oof = []
	count_oor = []
	count_on = []
	count_nn = []
	for i in range(len(mylist)-1):
		time.append(mylist[i+1])
		#oldE_plot.append(num_EW[z + i - 1983])
		string = '/home/upasana/upasana_dutta/research_work/Clean/AI/1969_'+str(mylist[i])+'.net'
		string2 = '/home/upasana/upasana_dutta/research_work/Clean/AI/1969_'+str(mylist[i+1])+'.net'	
		with open(string, 'r') as f:
			with open(string2, 'r') as f2:
				countOOR = 0
				countOOF = 0
				SAME = 0
				countON = 0
				countNN = 0
				mydict = {}
				p = f.readlines()
				for k in range(num_V[i]+1,len(p)):
					x = p[k]
					x_L = list(x.split())
					#print(len(x_L))
					mystr = x_L[0]+"_"+x_L[1]
					mydict[mystr] = int(x_L[2])
				p = f2.readlines()
				for k in range(num_V[i+1]+1,len(p)):
					x = p[k]
					x_L = list(x.split())
					#print(len(x_L))
					mystr = x_L[0]+"_"+x_L[1]
					if mystr in mydict.keys():
						if int(x_L[2]) == mydict[mystr]:
							SAME+=1
						elif int(x_L[2]) > mydict[mystr]:
							countOOR+=1
							
					else:
						if (int(x_L[0])>num_V[i ] and int(x_L[1])<=num_V[i ]) or (int(x_L[0])<=num_V[i ] and int(x_L[1])>num_V[i ]):
							countON+=int(x_L[2])
						if (int(x_L[0])<=num_V[i ] and int(x_L[1])<=num_V[i ]):
							countOOF+=int(x_L[2])
							
						if (int(x_L[0])>num_V[i ] and int(x_L[1])>num_V[i ]):
							countNN+=int(x_L[2])
				#added.append(w)
				same.append(countOOF+countOOR)
				count_oof.append(countOOF)				
				count_oor.append(countOOR)
				count_on.append(countON)						
				count_nn.append(countNN)
	#fig1=plt.plot(C_values, combined_score)
	plt.title("Timestamp vs. New Co-authorships")
	plt.xlabel('Timestamp')
	plt.ylabel('New Co-authorships ')
	plt.plot(time, count_oof, 'b-',label='Co-authership between Fresh Old-Old',marker='o', markerfacecolor='black', markersize=3)
	for a,b in zip(time, count_oof): 
    		plt.text(a, b, str(b),fontsize=7)
	plt.plot(time, count_oor, 'g-',label='Co-authership between Old-Old',marker='o', markerfacecolor='black', markersize=3)
	for a,b in zip(time, count_oor): 
    		plt.text(a, b, str(b),fontsize=7)
	plt.plot(time, count_on, 'y-',label='Co-authership between Old-New',marker='o', markerfacecolor='black', markersize=3)
	for a,b in zip(time, count_on): 
    		plt.text(a, b, str(b),fontsize=7)
	plt.plot(time, count_nn, 'r-',label='Co-authership between New-New',marker='o', markerfacecolor='black', markersize=3)
	for a,b in zip(time, count_nn): 
    		plt.text(a, b, str(b),fontsize=7)
	plt.legend()
	plt.show()
	#print(num_V)
	#print(count_nn)	
	return count_oof,count_oor,count_on,count_nn, num_V, num_EW,mylist
		
def alter_csv():
	with open('AI-Network_Family-metrics.csv', 'w') as f:
		writer =  csv.writer(f)
		x = numvertices()
		mylist = ['Timestamp']+x[6]
		l1 = ['old-old repeating',0]+x[1]
		l2 = ['old-old fresh',0]+x[0]
		l3 = ['old-new',0]+x[2]
		l4 = ['new-new',0]+x[3]
		l5 = ['vertices']+x[4]
		l6 = ['edges']+x[5]
		rows = zip(mylist,l5,l6,l1,l2,l3,l4)
		for row in rows:
			writer.writerow(row)
		
alter_csv()

